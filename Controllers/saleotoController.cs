﻿
using System.Collections.Generic;
using System.Linq;

public class SaleModel
{

    public SaleModel()
    {
        Products = new List<product>();
        Payments = new List<payment>();
        Discounts = new List<discount>();
    }
    #region Properties
    public string Code { get; set; }
    List<product> Products { get; set; }
    public decimal GrandTotal
    {
        get
        {
            return Products.Sum(s => s.Price);
        }
    }
    public decimal TotalQuantity
    {
        get
        {
            if (Products.Any())
                return Products.Sum(s => s.Quantity);

            return decimal.Zero;
        }
    }
    public List<payment> Payments { get; set; }


    public decimal TotalPayment
    {
        get
        {
            if (Payments.Any())
                return Payments.Sum(s => s.Price);
            return decimal.Zero;
        }
    }
    List<discount> Discounts { get; set; }

    public decimal TotalDiscount
    {
        get
        {
            if (Discounts.Any())
                return Discounts.Sum(s => s.Price);
            return decimal.Zero;
        }
    }

    /// Yapılan ödemeler ile toplam tutarın farkı.
            /// Kalan tutar

    public decimal RemainingPrice
    {
        get
        {
            return GrandTotal - (TotalPayment + TotalDiscount);
        }
    }
    #endregion
    #region Methods

    /// İlgili SaleModel'e bir satır ürün ekler..

    public void AddProduct(Product Product)
    {
        Products.Add(product);
    }

    /// İlgili SaleModel'e bir satır ödeme ekler..

    public void AddPayment(Payment payment)
    {
        Payments.Add(payment);
    }

    /// İlgili SaleModel'e bir satır indirim ekler..

    public void AddDiscount(Discount discount)
    {
        Discounts.Add(discount);
    }
}